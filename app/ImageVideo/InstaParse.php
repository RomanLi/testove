<?php

include $_SERVER['DOCUMENT_ROOT']. '/app/InstaItem.php';

class InstaParse {
    public $link;
    public $file;
    public $counter;
    public $next_url;

    public function __construct($link)
    {
        $this->link = $link;
        $this->counter = 0;
        $this->file = $this->getFile();
        $this->next_url = $this->getNextUrl();
    }

    public function getFile()
    {
        $json_st = file_get_contents($this->link);
        $file = json_decode($json_st, true);
        return $file;
    }

    public function getData()
    {
        $file = $this->file;
        $arrayOfElements = array();
        foreach ($file as $data) {
            if (key($data) == 'data') {
                foreach ($data as $dataObject) {
                    $object = new InstaItem();
                    ++$this->counter;
                    switch ($dataObject['type']) {
                        case 'image':
                            $object->thumbnail = $dataObject['images']['thumbnail']['url'];
                            $object->created_time = $dataObject['created_time'];
                            $object->standart_resolution = $dataObject['images']['standard_resolution']['url'];
                            $object->type = $dataObject['type'];
                            break;
                        case 'video':
                            $object->thumbnail = 'https://whatswp.com/wp-content/uploads/2014/02/wordpress-video-player-featured-150x150.jpg';
                            $object->created_time = $dataObject['created_time'];
                            $object->standart_resolution = $dataObject['videos']['standard_resolution']['url'];
                            $object->type = $dataObject['type'];
                            break;
                    }
                    array_push($arrayOfElements, $object);
                }
            }
        }
        return $arrayOfElements;
    }

    public function renderImage()
    {
        $arrayOfElements = $this->getData();
        $html = '';
        foreach ($arrayOfElements as $data) {
            $html .= '<div class="container">';
            $html .= '<img data-toggle="modal" data-target="#' . $data->created_time . '" src="' . $data->thumbnail . '" />';
            $html .= '<div class="modal fade" id="' . $data->created_time . '" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-footer">';
            $html .= date('M j, Y', $data->created_time);
            $html .= '</div><div class="modal-body">';
            if ($data->type == 'image') {
                $html .= '<img src="' . $data->standart_resolution . '" /></div></div></div></div></div>';
            } else {
                $html .= '<video controls><source src="' . $data->standart_resolution  . '"type="video/mp4" width="640",height="640"></video></video></div></div></div></div></div>';
            }
        }
        return $html;
    }

    public function checkNextUrlExist()
    {
        if ($this->file['pagination']['next_url']) {
            return true;
        }
        return false;
    }

    public function getNextUrl()
    {
        if (array_key_exists('next_url', $this->file['pagination'])) {
            $nextUrl = $this->file['pagination']['next_url'];
            return $nextUrl;
        }
        return false;
    }
}