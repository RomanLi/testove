<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/local.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<div id="content">

</div>
<div id="button">
    <button id="load" class="btn btn-default btn-lg">Load more</button>
</div>
    <script>
        var url=JSON.stringify("https://api.instagram.com/v1/users/2912979/media/recent/?access_token=2912979.87fdd31.0949d22f4a714349ae84643c5af165ef");
        $(document).ready(function(){
            $.ajax({
                type: "POST",
                url: "app/test.php",
                data: {
                    'action': 'onReady',
                    url
                },
                dataType: "json",
                success: function(response){
                    url=JSON.stringify(response.next_url);
                    $('#content').append(response.html);
                    window.scrollTo(0,document.body.scrollHeight);
                }
            });

            $('#load').click(function(){
                $.ajax({
                    type: "POST",
                    url: "app/test.php",
                    data: {
                        'action': 'loadMore',
                        'url': url
                    },
                    dataType: "json",
                    success: function(response){
                        console.log(response.next_url);
                        if (response.next_url) {
                            url = JSON.stringify(response.next_url);
                            $('#content').append(response.html);
                            window.scrollTo(0,document.body.scrollHeight);
                        } else {
                            $('#load').prop('disabled', true);
                            alert('Sorry, no more pages');
                        }
                    }
                });
            });
        });
    </script>
</body>
</html>
